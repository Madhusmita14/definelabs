package com.calculation.definelab_task.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.calculation.definelab_task.*
import com.calculation.definelab_task.databinding.FragmentHomeBinding
import org.json.JSONException
import org.json.JSONObject
import kotlin.reflect.typeOf

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    lateinit var recyclerView: RecyclerView
    lateinit var adapter: CustomAdapter
    lateinit var allMatchesList: ArrayList<AllMatchesModel>

    lateinit var model: AllMatchesModel

    private lateinit var databaseHandler: DatabaseHandler
    lateinit var animationView: LottieAnimationView

    private val all_matches_URL =
        "https://api.foursquare.com/v2/venues/search?ll=40.7484,-73.9857&oauth_token=NPKYZ3WZ1VYMNAZ2FLX1WLECAWSMUVOQZOIDBN53F3LVZBPQ&v=20180616"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        recyclerView = binding.recyclerview
        recyclerView.layoutManager = LinearLayoutManager(context)

        // ArrayList of class ItemsViewModel
        allMatchesList = ArrayList<AllMatchesModel>()

//        adapter = CustomAdapter(requireActivity(), allMatchesList)
//        adapter?.setOnClickItem {
//            Toast.makeText(context,""+it.title,Toast.LENGTH_SHORT).show()
//        }

        databaseHandler = DatabaseHandler(requireContext())

        all_matches_Api()

        animationView = binding.img
        animationView.playAnimation()
        animationView.visibility = View.VISIBLE

        return root
    }

    private fun all_matches_Api() {
        HttpsTrustManager.allowAllSSL()
        val stringRequest = StringRequest(
            Request.Method.GET, all_matches_URL,
            { response ->
                animationView.visibility = View.GONE
                try {
                    val jsonObject = JSONObject(response)
                    val jsonObject1 = jsonObject.getJSONObject("response")
                    val array = jsonObject1.getJSONArray("venues")

                    for (i in 0 until array.length()) {
                        var jsonObject2 = array.getJSONObject(i)

                        var user_id = jsonObject2.getString("id")
                        var title = jsonObject2.getString("name")

                        model = AllMatchesModel(user_id, title, "101 Sector 5","5km","1234567890")

                        allMatchesList.add(model)

                    }

                    adapter = CustomAdapter(requireActivity(), allMatchesList)
                    recyclerView.adapter = adapter

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        ) { error -> Toast.makeText(requireContext(), "" + error, Toast.LENGTH_SHORT).show() }

        val requestQueue = Volley.newRequestQueue(requireContext())
        requestQueue.add(stringRequest)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
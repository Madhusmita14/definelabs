package com.calculation.definelab_task.ui.gallery

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.calculation.definelab_task.*
import com.calculation.definelab_task.databinding.FragmentGalleryBinding

class GalleryFragment : Fragment() {

    lateinit var recyclerView: RecyclerView
    private var adapter: SavedMatchesAdapter? = null

    private var _binding: FragmentGalleryBinding? = null

    private lateinit var databaseHandler: DatabaseHandler

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentGalleryBinding.inflate(inflater, container, false)
        val root: View = binding.root

        recyclerView = binding.recyclerview1
        recyclerView.layoutManager = LinearLayoutManager(context)

        adapter = SavedMatchesAdapter()
        recyclerView.adapter = adapter

        adapter?.setOnClickItem {
            //Toast.makeText(context,""+it.id,Toast.LENGTH_SHORT).show()
        }

        adapter?.setOnClickDeleteItem {
            deleteRecord(it.id)
        }
        databaseHandler = DatabaseHandler(requireContext())

        viewRecord()
        
        return root
    }

    private fun viewRecord() {
        val savedList = databaseHandler.viewSavedMatches()
        adapter?.addItems(savedList)
        Log.d("savedlist","${savedList}")
    }

    private fun deleteRecord(id: String){
        if (id == null) return
        databaseHandler.deleteSavedMatches(id)
        Log.d("id","${id}")
        viewRecord()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
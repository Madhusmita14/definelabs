package com.calculation.definelab_task

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

class CustomAdapter(private val context: Activity, private var mList: List<AllMatchesModel>) : RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    private lateinit var databaseHandler: DatabaseHandler

    lateinit var all_match: String

    private var onClickItem: ((AllMatchesModel) -> Unit)? = null

    fun setOnClickItem(callback: (AllMatchesModel) -> Unit) {
        this.onClickItem = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.all_match_row, parent, false)

        return ViewHolder(view)
    }

    private fun addToSavedMatches(allMatchesModel: AllMatchesModel) {
        val saved = SavedModelClass(title = allMatchesModel.title,address = allMatchesModel.address,
            distance = allMatchesModel.distance,phone = allMatchesModel.phone)
        val status = databaseHandler.addSavedMatches(saved)

        if (status > -1) {
            Toast.makeText(context, "Item is saved", Toast.LENGTH_SHORT).show()
            notifyDataSetChanged()
        } else {
            Toast.makeText(context, "Something Error or Item already saved", Toast.LENGTH_SHORT).show()
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var allMatchesModel = mList[position]
        holder.bindView(allMatchesModel)

        holder.textView.text = allMatchesModel?.title
        holder.location_tv.text = allMatchesModel?.address
        holder.direction_tv.text = allMatchesModel?.distance
        holder.phone_tv.text = allMatchesModel?.phone

        databaseHandler = DatabaseHandler(context)

        holder.add_to_fav.setOnClickListener {
            holder.add_to_fav.setImageResource(R.drawable.savedmatches_star)
            addToSavedMatches(allMatchesModel)

/*            if(holder.add_to_fav.get == R.drawable.allmatches_star){
                holder.add_to_fav.setImageResource(R.drawable.savedmatches_star)
                addToSavedMatches(allMatchesModel)
            }
            else{
                all_match = mList.get(position).title
                val savedList = databaseHandler.viewEmployee()
                for (j in 0 until savedList.size) {
                    var saved_match = savedList.get(j).title
                    if (all_match == saved_match) {
                        Log.d("savedlist_home","${all_match +":"+ saved_match}")
                        Log.d("deleteRecord","${savedList.get(j).id}")
                        deleteRecord(savedList.get(j).id)
                        notifyDataSetChanged()
                    }
                }
            }*/

        }

        holder.itemView.setOnClickListener {
            onClickItem?.invoke(allMatchesModel)
        }

        val savedList = databaseHandler.viewSavedMatches()
        all_match = mList.get(position).title

        var flag: Boolean = false
        for (j in 0 until savedList.size) {
            var saved_match = savedList.get(j).title
            if (all_match == saved_match) {
                Log.d("savedlist_home","${all_match +":"+ saved_match}")
                holder.add_to_fav.setImageResource(R.drawable.savedmatches_star)
                flag = true
            }
        }
        if(flag == false){
            holder.add_to_fav.setImageResource(R.drawable.allmatches_star)
            holder.textView.text = mList.get(position).title
        }
    }

    private fun deleteRecord(id: String){
        if (id == null) return
        databaseHandler.deleteSavedMatches(id)
        Log.d("id","${id}")
    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }

    class ViewHolder(ItemView: View): RecyclerView.ViewHolder(ItemView){
        var textView: TextView = itemView.findViewById(R.id.textView)
        var add_to_fav: ImageView = itemView.findViewById(R.id.add_to_fav)
        var location_tv: TextView = itemView.findViewById(R.id.location_tv)
        var direction_tv: TextView = itemView.findViewById(R.id.direction_tv)
        var phone_tv: TextView = itemView.findViewById(R.id.phone_tv)

        fun bindView(allMatchesModel: AllMatchesModel){
            textView.text = allMatchesModel.title
            location_tv.text = allMatchesModel.address
            direction_tv.text = allMatchesModel.distance
            phone_tv.text = allMatchesModel.phone
        }
    }
}
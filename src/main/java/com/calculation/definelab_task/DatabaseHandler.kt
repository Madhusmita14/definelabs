package com.calculation.definelab_task

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper

class DatabaseHandler(context: Context): SQLiteOpenHelper(context,DATABASE_NAME,null,DATABASE_VERSION) {

    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "SavedMatchesDatabase"
        private val TABLE_SAVED = "SavedTable"
        private val KEY_ID = "id"
        private val KEY_TITLE = "title"
        private val KEY_ADDRESS = "address"
        private val KEY_DISTANCE = "distance"
        private val KEY_PHONE = "phone"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        //creating table with fields
        val CREATE_SAVED_TABLE = ("CREATE TABLE " + TABLE_SAVED + "(" + KEY_ID + " TEXT," + KEY_TITLE + " TEXT PRIMARY KEY UNIQUE," +
                KEY_ADDRESS + " TEXT," +  KEY_DISTANCE + " TEXT," + KEY_PHONE + " TEXT" + ")" )

        db?.execSQL(CREATE_SAVED_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        db!!.execSQL("DROP TABLE IF EXISTS " + TABLE_SAVED)
        onCreate(db)
    }

    //method to insert data
    fun addSavedMatches(emp: SavedModelClass):Long{
        val db = this.writableDatabase

        val contentValues = ContentValues()
        contentValues.put(KEY_ID, emp.id)
        contentValues.put(KEY_TITLE, emp.title)
        contentValues.put(KEY_ADDRESS,emp.address)
        contentValues.put(KEY_DISTANCE,emp.distance)
        contentValues.put(KEY_PHONE,emp.phone)

        // Inserting Row
        val success = db.insert(TABLE_SAVED, null, contentValues)

        db.close() // Closing database connection
        return success
    }

    //method to read data
    fun viewSavedMatches():ArrayList<SavedModelClass>{
        val empList:ArrayList<SavedModelClass> = ArrayList<SavedModelClass>()

        val selectQuery = "SELECT  * FROM $TABLE_SAVED"
        val db = this.readableDatabase

        var cursor: Cursor? = null
        try{
            cursor = db.rawQuery(selectQuery, null,null)
        }catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }

        var id: String
        var title: String
        var address: String
        var distance: String
        var phone: String

        if (cursor.moveToFirst()) {
            do {
                id = cursor.getString(cursor.getColumnIndex("id"))
                title = cursor.getString(cursor.getColumnIndex("title"))
                address = cursor.getString(cursor.getColumnIndex("address"))
                distance = cursor.getString(cursor.getColumnIndex("distance"))
                phone = cursor.getString(cursor.getColumnIndex("phone"))

                val emp= SavedModelClass(id = id, title = title,address = address,distance = distance,phone = phone)
                empList.add(emp)
            } while (cursor.moveToNext())
        }
        return empList
    }

    //delete record
    fun deleteSavedMatches(id: String):Int{
        val db = this.writableDatabase

        val contentValues = ContentValues()
        contentValues.put(KEY_ID, id)

        // Deleting Row
        val success = db.delete(TABLE_SAVED,"id=$id" ,null)

        db.close()
        return success
    }
}
package com.calculation.definelab_task

data class SavedModelClass
    (var id:String = getAutoID().toString(), val title:String, val address:String, val distance: String,val phone: String){

    companion object{
        fun getAutoID():Int{
            val random = java.util.Random()
            return random.nextInt(100)
        }
    }
}
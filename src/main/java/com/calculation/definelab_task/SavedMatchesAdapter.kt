package com.calculation.definelab_task

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class SavedMatchesAdapter: RecyclerView.Adapter<SavedMatchesAdapter.SavedViewHolder>() {

    private var savedList: ArrayList<SavedModelClass> = ArrayList()
    private var onClickItem: ((SavedModelClass) -> Unit)? = null
    private var onClickDeleteItem: ((SavedModelClass) -> Unit)? = null

    fun addItems(items: ArrayList<SavedModelClass>){
        this.savedList = items
        notifyDataSetChanged()
    }

    fun setOnClickItem(callback: (SavedModelClass) -> Unit){
        this.onClickItem = callback
    }

    fun setOnClickDeleteItem(callback: (SavedModelClass) -> Unit){
        this.onClickDeleteItem = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)=  SavedViewHolder (
        LayoutInflater.from(parent.context).inflate(R.layout.saved_matches_row,parent,false)
    )

    override fun onBindViewHolder(holder: SavedViewHolder, position: Int) {
        var saved = savedList[position]
        holder.bindView(saved)

        holder.itemView.setOnClickListener {
            onClickItem?.invoke(saved)
        }

        holder.add_to_fav1.setOnClickListener {
            onClickDeleteItem?.invoke(saved)
        }
    }

    override fun getItemCount(): Int {
       return savedList.size
    }

    class SavedViewHolder(var view: View): RecyclerView.ViewHolder(view){
        var textView1 = view.findViewById<TextView>(R.id.textView1)
        var add_to_fav1 = view.findViewById<ImageView>(R.id.add_to_fav1)
        var location_tv1 = view.findViewById<TextView>(R.id.location_tv1)
        var direction_tv1 = view.findViewById<TextView>(R.id.direction_tv1)
        var phone_tv1 = view.findViewById<TextView>(R.id.phone_tv1)

        fun bindView(savedModelClass: SavedModelClass){
            textView1.text = savedModelClass.title
            location_tv1.text = savedModelClass.address
            direction_tv1.text = savedModelClass.distance
            phone_tv1.text = savedModelClass.phone
        }
    }
}